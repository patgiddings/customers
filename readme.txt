* Project: customers
* Author: D. Patrick Giddings
* Date: 2017-05-25


A single page web application that allows a user to Create, List, Update and
Delete customers.

A customer has the following structure:
{
  Name,
  Email,
  Telephone,
  Address: {
    Street,
    City,
    State,
    Zip
  }
}

The web application uses .json files as the storage mechanism for each customer
record.
