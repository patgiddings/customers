class Address {
  constructor(pojo) {
    this.street = pojo ? pojo.street : "";
    this.city = pojo ? pojo.city : "";
    this.state = pojo ? pojo.state : "";
    this.zip = pojo ? pojo.zip : "";
  }

  toString() {
    return this.street + ', ' + this.city + ', ' + this.state + ' ' + this.zip;
  }
}
