class Customer {
  constructor(json) {
    let pojo;
    if (json) {
      pojo = JSON.parse(json);
    }
    this.id = pojo ? pojo.id : -1;
    this.name = pojo ? pojo.name : "";
    this.email = pojo ? pojo.email : "";
    this.telephone = pojo ? pojo.telephone : "";
    this.address = pojo ? new Address(pojo.address) : new Address();
  }

  toString() {
    return this.name + ' - ' + this.email + ' - ' + this.telephone + ' - ' + this.address.toString();
  }
}
