angular.module('Customers', []);
;/*
 * Factory service for reading and writing files from FileSystem api
 */
 angular.module('Customers').factory('FSService', ['$q', ($q) => {
   const sz = 5*1024*1024;  // 5mb
   const cs = new ChromeStore([
     { path: 'customers' }
   ]);
   const initDeferred = $q.defer();
   const initPromise = initDeferred.promise;

   let allCustomers = [];
   let initialized = false;

   function deleteAllCustomers() {
     initPromise.then(function() {
       listFiles().then((all) => {
         for (let i = 0; i < all.length; i++) {
           cs.deleteFile('customers/' + all[i].name);
         }
       });
     });
   }

   function deleteCustomer(id) {
     initPromise.then(function() {
       cs.deleteFile('customers/' + id + '.json');
     });
   }

   function initStore() {
     cs.init(sz, () => {
       initialized = true;
       initDeferred.resolve();
     });
   }

   function listFiles() {
     const d = $q.defer();
     initPromise.then(function() {
       cs.ls('customers', (arr) => {
         d.resolve(arr);
       });
     });
     return d.promise;
   }

   function loadCustomer(id) {
     const d = $q.defer();
     initPromise.then(function() {
       cs.getFile('customers/' + id + '.json',
                  {create: false, exclusive: true},
                  (fileEntry) => {
         fileEntry.file((file) => {
           var reader = new FileReader();
           reader.onload = () => {
             var json = reader.result;
             var customer = new Customer(json);
             d.resolve(customer);
           };
           reader.readAsText(file);
         });
       });
     });
     return d.promise;
   }

   function loadAllCustomers() {
     const d = $q.defer();
     const promises = [];
     initPromise.then(function() {
       listFiles().then((all) => {
         for (let i = 0; i < all.length; i++) {
           promises.push(loadCustomer(all[i].name.substring(0, all[i].name.length - 5)));
         }
         $q.all(promises).then((list) => {
           d.resolve(list);
         }, d.reject);
       });
     });
     return d.promise;
   }

   function saveCustomer(customer) {
     const d = $q.defer();
     initPromise.then(function() {
       cs.write('customers/' + customer.id + '.json',
                'application/json',
                angular.toJson(customer),
                {create: true},
                d.resolve,
                d.reject);
     });
     return d.promise;
   }

   initStore();

   return {
     deleteAllCustomers: deleteAllCustomers,
     deleteCustomer: deleteCustomer,
     listFiles: listFiles,
     loadAllCustomers: loadAllCustomers,
     loadCustomer: loadCustomer,
     saveCustomer: saveCustomer
   };
 }]);
;angular.module('Customers').controller('list', ['$scope', 'FSService', function($scope, fsService) {
  var self = this;
  self.customer = new Customer();
  self.customers = [];
  self.nextId = 0;
  self.showNewCustomerForm = false;

  fsService.loadAllCustomers().then((list) => {
    self.customers = list;
    self.nextId = list.length ? Math.max.apply(Math, list.map((o) => { return o.id })) + 1 : 0;
  });

  self.addCustomer = function() {
    let newCustomer = false;
    if (self.customer.id === -1) {
      self.customer.id = self.nextId++;
      newCustomer = true;
    }
    fsService.saveCustomer(self.customer).then(() => {
      newCustomer && self.customers.push(self.customer);
      self.showNewCustomerForm = false;
      self.customer = new Customer();
    }, console.error);
  }

  self.clear = function() {
    self.customer = new Customer();
    $scope.form.$setPristine();
  }

  self.deleteAll = function() {
    self.customers = [];
    fsService.deleteAllCustomers();
  };

  self.deleteCustomer = function(customer) {
    self.customers.splice(self.customers.indexOf(customer), 1);
    fsService.deleteCustomer(customer.id);
  };

  self.editCustomer = function(customer) {
    self.customer = customer;
    self.showNewCustomerForm = true;
  };

  self.hideCustomer = function() {
    self.showNewCustomerForm = false;
  }

  self.newCustomer = function() {
    self.clear();
    self.showNewCustomerForm = true;
  }

}]);
;class Address {
  constructor(pojo) {
    this.street = pojo ? pojo.street : "";
    this.city = pojo ? pojo.city : "";
    this.state = pojo ? pojo.state : "";
    this.zip = pojo ? pojo.zip : "";
  }

  toString() {
    return this.street + ', ' + this.city + ', ' + this.state + ' ' + this.zip;
  }
}
;class Customer {
  constructor(json) {
    let pojo;
    if (json) {
      pojo = JSON.parse(json);
    }
    this.id = pojo ? pojo.id : -1;
    this.name = pojo ? pojo.name : "";
    this.email = pojo ? pojo.email : "";
    this.telephone = pojo ? pojo.telephone : "";
    this.address = pojo ? new Address(pojo.address) : new Address();
  }

  toString() {
    return this.name + ' - ' + this.email + ' - ' + this.telephone + ' - ' + this.address.toString();
  }
}
