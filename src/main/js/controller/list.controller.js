angular.module('Customers').controller('list', ['$scope', 'FSService', function($scope, fsService) {
  var self = this;
  self.customer = new Customer();
  self.customers = [];
  self.nextId = 0;
  self.showNewCustomerForm = false;

  fsService.loadAllCustomers().then((list) => {
    self.customers = list;
    self.nextId = list.length ? Math.max.apply(Math, list.map((o) => { return o.id })) + 1 : 0;
  });

  self.addCustomer = function() {
    let newCustomer = false;
    if (self.customer.id === -1) {
      self.customer.id = self.nextId++;
      newCustomer = true;
    }
    fsService.saveCustomer(self.customer).then(() => {
      newCustomer && self.customers.push(self.customer);
      self.showNewCustomerForm = false;
      self.customer = new Customer();
    }, console.error);
  }

  self.clear = function() {
    self.customer = new Customer();
    $scope.form.$setPristine();
  }

  self.deleteAll = function() {
    self.customers = [];
    fsService.deleteAllCustomers();
  };

  self.deleteCustomer = function(customer) {
    self.customers.splice(self.customers.indexOf(customer), 1);
    fsService.deleteCustomer(customer.id);
  };

  self.editCustomer = function(customer) {
    self.customer = customer;
    self.showNewCustomerForm = true;
  };

  self.hideCustomer = function() {
    self.showNewCustomerForm = false;
  }

  self.newCustomer = function() {
    self.clear();
    self.showNewCustomerForm = true;
  }

}]);
