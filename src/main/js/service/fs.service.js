/*
 * Factory service for reading and writing files from FileSystem api
 */
 angular.module('Customers').factory('FSService', ['$q', ($q) => {
   const sz = 5*1024*1024;  // 5mb
   const cs = new ChromeStore([
     { path: 'customers' }
   ]);
   const initDeferred = $q.defer();
   const initPromise = initDeferred.promise;

   let allCustomers = [];
   let initialized = false;

   function deleteAllCustomers() {
     initPromise.then(function() {
       listFiles().then((all) => {
         for (let i = 0; i < all.length; i++) {
           cs.deleteFile('customers/' + all[i].name);
         }
       });
     });
   }

   function deleteCustomer(id) {
     initPromise.then(function() {
       cs.deleteFile('customers/' + id + '.json');
     });
   }

   function initStore() {
     cs.init(sz, () => {
       initialized = true;
       initDeferred.resolve();
     });
   }

   function listFiles() {
     const d = $q.defer();
     initPromise.then(function() {
       cs.ls('customers', (arr) => {
         d.resolve(arr);
       });
     });
     return d.promise;
   }

   function loadCustomer(id) {
     const d = $q.defer();
     initPromise.then(function() {
       cs.getFile('customers/' + id + '.json',
                  {create: false, exclusive: true},
                  (fileEntry) => {
         fileEntry.file((file) => {
           var reader = new FileReader();
           reader.onload = () => {
             var json = reader.result;
             var customer = new Customer(json);
             d.resolve(customer);
           };
           reader.readAsText(file);
         });
       });
     });
     return d.promise;
   }

   function loadAllCustomers() {
     const d = $q.defer();
     const promises = [];
     initPromise.then(function() {
       listFiles().then((all) => {
         for (let i = 0; i < all.length; i++) {
           promises.push(loadCustomer(all[i].name.substring(0, all[i].name.length - 5)));
         }
         $q.all(promises).then((list) => {
           d.resolve(list);
         }, d.reject);
       });
     });
     return d.promise;
   }

   function saveCustomer(customer) {
     const d = $q.defer();
     initPromise.then(function() {
       cs.write('customers/' + customer.id + '.json',
                'application/json',
                angular.toJson(customer),
                {create: true},
                d.resolve,
                d.reject);
     });
     return d.promise;
   }

   initStore();

   return {
     deleteAllCustomers: deleteAllCustomers,
     deleteCustomer: deleteCustomer,
     listFiles: listFiles,
     loadAllCustomers: loadAllCustomers,
     loadCustomer: loadCustomer,
     saveCustomer: saveCustomer
   };
 }]);
