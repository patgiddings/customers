'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

angular.module('Customers', []);
; /*
  * Factory service for reading and writing files from FileSystem api
  */
angular.module('Customers').factory('FSService', ['$q', function ($q) {
  var sz = 5 * 1024 * 1024; // 5mb
  var cs = new ChromeStore([{ path: 'customers' }]);
  var initDeferred = $q.defer();
  var initPromise = initDeferred.promise;

  var allCustomers = [];
  var initialized = false;

  function deleteAllCustomers() {
    initPromise.then(function () {
      listFiles().then(function (all) {
        for (var i = 0; i < all.length; i++) {
          cs.deleteFile('customers/' + all[i].name);
        }
      });
    });
  }

  function deleteCustomer(id) {
    initPromise.then(function () {
      cs.deleteFile('customers/' + id + '.json');
    });
  }

  function initStore() {
    cs.init(sz, function () {
      initialized = true;
      initDeferred.resolve();
    });
  }

  function listFiles() {
    var d = $q.defer();
    initPromise.then(function () {
      cs.ls('customers', function (arr) {
        d.resolve(arr);
      });
    });
    return d.promise;
  }

  function loadCustomer(id) {
    var d = $q.defer();
    initPromise.then(function () {
      cs.getFile('customers/' + id + '.json', { create: false, exclusive: true }, function (fileEntry) {
        fileEntry.file(function (file) {
          var reader = new FileReader();
          reader.onload = function () {
            var json = reader.result;
            var customer = new Customer(json);
            d.resolve(customer);
          };
          reader.readAsText(file);
        });
      });
    });
    return d.promise;
  }

  function loadAllCustomers() {
    var d = $q.defer();
    var promises = [];
    initPromise.then(function () {
      listFiles().then(function (all) {
        for (var i = 0; i < all.length; i++) {
          promises.push(loadCustomer(all[i].name.substring(0, all[i].name.length - 5)));
        }
        $q.all(promises).then(function (list) {
          d.resolve(list);
        }, d.reject);
      });
    });
    return d.promise;
  }

  function saveCustomer(customer) {
    var d = $q.defer();
    initPromise.then(function () {
      cs.write('customers/' + customer.id + '.json', 'application/json', angular.toJson(customer), { create: true }, d.resolve, d.reject);
    });
    return d.promise;
  }

  initStore();

  return {
    deleteAllCustomers: deleteAllCustomers,
    deleteCustomer: deleteCustomer,
    listFiles: listFiles,
    loadAllCustomers: loadAllCustomers,
    loadCustomer: loadCustomer,
    saveCustomer: saveCustomer
  };
}]);
;angular.module('Customers').controller('list', ['$scope', 'FSService', function ($scope, fsService) {
  var self = this;
  self.customer = new Customer();
  self.customers = [];
  self.nextId = 0;
  self.showNewCustomerForm = false;

  fsService.loadAllCustomers().then(function (list) {
    self.customers = list;
    self.nextId = list.length ? Math.max.apply(Math, list.map(function (o) {
      return o.id;
    })) + 1 : 0;
  });

  self.addCustomer = function () {
    var newCustomer = false;
    if (self.customer.id === -1) {
      self.customer.id = self.nextId++;
      newCustomer = true;
    }
    fsService.saveCustomer(self.customer).then(function () {
      newCustomer && self.customers.push(self.customer);
      self.showNewCustomerForm = false;
      self.customer = new Customer();
    }, console.error);
  };

  self.clear = function () {
    self.customer = new Customer();
    $scope.form.$setPristine();
  };

  self.deleteAll = function () {
    self.customers = [];
    fsService.deleteAllCustomers();
  };

  self.deleteCustomer = function (customer) {
    self.customers.splice(self.customers.indexOf(customer), 1);
    fsService.deleteCustomer(customer.id);
  };

  self.editCustomer = function (customer) {
    self.customer = customer;
    self.showNewCustomerForm = true;
  };

  self.hideCustomer = function () {
    self.showNewCustomerForm = false;
  };

  self.newCustomer = function () {
    self.clear();
    self.showNewCustomerForm = true;
  };
}]);
;
var Address = function () {
  function Address(pojo) {
    _classCallCheck(this, Address);

    this.street = pojo ? pojo.street : "";
    this.city = pojo ? pojo.city : "";
    this.state = pojo ? pojo.state : "";
    this.zip = pojo ? pojo.zip : "";
  }

  _createClass(Address, [{
    key: 'toString',
    value: function toString() {
      return this.street + ', ' + this.city + ', ' + this.state + ' ' + this.zip;
    }
  }]);

  return Address;
}();

;
var Customer = function () {
  function Customer(json) {
    _classCallCheck(this, Customer);

    var pojo = void 0;
    if (json) {
      pojo = JSON.parse(json);
    }
    this.id = pojo ? pojo.id : -1;
    this.name = pojo ? pojo.name : "";
    this.email = pojo ? pojo.email : "";
    this.telephone = pojo ? pojo.telephone : "";
    this.address = pojo ? new Address(pojo.address) : new Address();
  }

  _createClass(Customer, [{
    key: 'toString',
    value: function toString() {
      return this.name + ' - ' + this.email + ' - ' + this.telephone + ' - ' + this.address.toString();
    }
  }]);

  return Customer;
}();
